package com;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class Mylistner implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent event) {

		System.out.println("destroyed");
		
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		
		ServletContext c=event.getServletContext();
		
		c.setAttribute("abc", "shubhbam");
		
		
		System.out.println("initialized");
		
	}

}
